let result = 0;


function createRandomInt(size) {
    return Math.floor(Math.random() * (10 ** size));
}

function createRandomIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function createRandomOperation() {
    const operations = ['+', '-', '*', '/'];
    return operations[Math.floor(Math.random() * operations.length)];
}

function createMathProblem() {
    const firstNumber = createRandomInt(createRandomIntInRange(1, 3));
    let secondNumber = createRandomInt(createRandomIntInRange(1, 3));
    
    const operation = createRandomOperation();

    if (operation === '/' && secondNumber < 2) {
        secondNumber = 2;
    }

    const decimal = 2;
    const multiplier = 10 ** decimal;

    result = Math.round((getResult(firstNumber, secondNumber, operation) + Number.EPSILON) * multiplier) / multiplier
    // round to second decimal place

    return `${firstNumber} ${operation} ${secondNumber}`;
}

function getResult(firstNumber, secondNumber, operation) {
    switch (operation) {
        case '+':
            return firstNumber + secondNumber;
        case '-':
            return firstNumber - secondNumber;
        case '*':
            return firstNumber * secondNumber;
        case '/':
            return firstNumber / secondNumber;
    }
    return -1;
}