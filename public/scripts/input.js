//Get the button elements that are the children of input-buttons and add a click listener
const buttons = document.querySelector('#input-buttons').children;
for (let i = 0; i < buttons.length; i++) {
buttons[i].addEventListener("click", function() {
    const input = document.querySelector('[data-dom-state="input-1"]');
    input.value += buttons[i].innerHTML;
});
}