let round = 0;
let timerInterval;
let maxScore = 20;
let score = 0;

function startCountdown() {
    // users get 20 seconds to enter an answer
    let countdown = 20;
    round += 1;

    timerInterval = setInterval(function () {
        // change timer label
        x = document.getElementsByClassName('time')
        for (var i = 0; i < x.length; i++) {
            x[i].innerText = countdown;
        }
        // only 5 questions till the game ends 
        if (round === 6) {
            // game ends 
            // go on to score page
            const achievedScore = score;
            if (statisticsHelper)
                statisticsHelper.endRun(achievedScore);
            clearInterval(timerInterval)
            alert(`5 rounds played, your score is: ${achievedScore}`);
            window.location.replace("./report-page.html")
        }
        if (countdown === 0) {
            // when timer stops before entering an answer -> reduce score
            clearInterval(timerInterval);
            score -= 3;
            x = document.getElementsByClassName('currentScore');
            for (var i = 0; i < x.length; i++) {
                x[i].innerText = score;
            }
            document.querySelector('.input').style.backgroundColor = "gray";
            setTimeout(function () {
                maxScore = 20;
                document.querySelector('.input').style.backgroundColor = "#333";
                clearInput();
                startCountdown();
                nextQuest();
            }, 1000);
        } else {
            countdown -= 1;
            maxScore -= 1;
        }
    }, 1000);
}

function scoreAnswerCorrect(question, answer) {
    // if right answer increase score and restart timer
    score += (maxScore + 1)
    clearInterval(timerInterval)
    startCountdown()
    afterEnter("green")
    x = document.getElementsByClassName('currentScore')
    for (var i = 0; i < x.length; i++) {
        x[i].innerText = score
    }
    maxScore = 20
    if (statisticsHelper)
        statisticsHelper.progressRun(question, answer, true, maxScore + 1, score);
}

function scoreAnswerIncorrect() {
    // if wrong answer reduce score and restart timer
    score -= 3
    clearInterval(timerInterval)
    startCountdown()
    afterEnter("red")
    x = document.getElementsByClassName('currentScore')
    for (var i = 0; i < x.length; i++) {
        x[i].innerText = score
    }
    maxScore = 20
}

function afterEnter(colour) {
    // give feedback to input and go on to next question
    document.querySelector('.input').style.backgroundColor = colour
    setTimeout(function () {
        document.querySelector('.input').style.backgroundColor = "#333"
        nextQuest()
        clearInput()
    }, 1500)
}

function clearInput() {
    // to clear input after entering
    document.querySelector('.input').value = ''
}