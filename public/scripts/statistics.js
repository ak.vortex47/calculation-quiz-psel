function getCookie(key) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${key}=`);
    if (parts.length === 2) {
        const part = parts.pop()
        if (part !== undefined)
            return part.split(';').shift();
    }
    return undefined;
}

function setCookie(key, value, sameSite = false) {
    if (sameSite)
        document.cookie = `${key}=${value}; path=/;SameSite=Lax`;
    else
        document.cookie = `${key}=${value}; path=/;SameSite=None;`;
}

class StatisticsHelper {
    statistics = JSON.parse(getCookie('statistics') || '{}');
    currentRun = {};

    constructor() {
        this.statistics.runs = this.statistics.runs || [];
        this.updateCookie();
    }

    updateCookie() {
        setCookie('statistics', JSON.stringify(this.statistics), true);
    }

    startRun(type, name) {
        this.currentRun = {
            userName: name,
            type: type,
            results: [],
            started: Date.now(),
            ended: -1,
            total_score: 0,
        };
        this.updateCookie();
    }
    
    endRun(total_score) {
        this.currentRun.ended = Date.now();
        this.currentRun.total_score = total_score;
        this.statistics.runs.push(this.currentRun);
        console.log("ENDING RUN");
        this.updateCookie();
    }
    
    progressRun(question, answer, correct, points_scored, points_total) {
        this.currentRun.results.push({
            question: question,
            answer: answer,
            correct: correct,
            points_scored: points_scored,
            points_total: points_total,
            timestamp: Date.now()
        });
    }

    deleteAll() {
        setCookie('statistics', JSON.stringify({}), true);
    }
}


